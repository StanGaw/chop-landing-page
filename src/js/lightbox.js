(()=>{
    const lightbox = document.createElement('div');
    const body = document.querySelector('body');
    const images  = document.querySelectorAll('img');

    lightbox.id = "lightbox";
    document.body.appendChild(lightbox);

    images.forEach(el=>{
        el.addEventListener('click',(e)=>{
            lightbox.classList.toggle('active');
            body.classList.toggle('overflow-hidden');
            lightbox.innerHTML=`
            <div class="lightbox-img js-lightbox-img">
                <img src='${e.target.src}'></img>
                
                <div class="closing"></div>
            </div>
            `
            const lightboxImg = document.querySelector('.js-lightbox-img img');
            const closing = document.querySelector('.closing');
            [lightboxImg,closing].forEach(el=>{
                el.addEventListener('click',()=>{
                    lightbox.classList.toggle('active');
                    body.classList.toggle('overflow-hidden');
                })
            })
        })
    })
})()