(()=>{
    const header = document.querySelector('.header');
    const body = document.querySelector('body');
    if(header){
        const hamburger = header.querySelector('.js-hamburger');


        hamburger.addEventListener('click',()=>{
            header.classList.toggle('active');;
            body.classList.toggle('overflow-hidden');
        })
    }
})()